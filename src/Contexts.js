import { createContext } from "react";

export const CommonContext = createContext();
export const AuthContext = createContext();
