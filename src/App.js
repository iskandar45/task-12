import { useContext, useState } from "react";
import { Home } from "./Pages/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { CommonProvider } from "./Components/CommonProvider";
import { About } from "./Pages/About";
import { Login } from "./Pages/Login";
import { AuthContext } from "./Contexts";
import { Redirect } from "react-router-dom";

export default function App() {
  const { isLoggedIn } = useContext(AuthContext);
  return (
    <Router>
      <Switch>
        <Route exact path="/login">
          <Login />
        </Route>
        <CommonProvider>
          <Route exact path="/">
            {!isLoggedIn ? <Redirect to="/login" /> : <Home />}
          </Route>
          <Route exact path="/about">
            {!isLoggedIn ? <Redirect to="/login" /> : <About />}
          </Route>
        </CommonProvider>
      </Switch>
    </Router>
  );
}
